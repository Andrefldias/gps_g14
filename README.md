# Project Title

## Contents
- [Team](#team)
- [Vision and Scope](#vision-and-scope)
- [Requirements](#requirements)
    - [Use case diagram](#use-case-diagram)
    - [User stories and prototypes](#user-stories-and-prototypes)
- [Architecture and Design](#architecture-and-design)
    - [Domain Model](#domain-model)
- [Implementation](#implementation)
    - [Product Increment 1](#product-increment-1)
    - [Product Increment 2](#product-increment-2)
    - [Product Increment 3](#product-increment-3)
    - [Product Increment 4](#product-increment-4)

## Team

- Leonardo Gomes - 2019133504
- João Santos - 2020136093
- João Duarte - 2020122715
- André Dias - 2021140917
- Bruno Gomes - 2017015265

***

## Vision and Scope

#### Problem Statement
##### Project background

As a referee, one of the problems I encounter is the difficulty in managing referees and assigning them to existing games. In some associations, it is as simple as sending a message asking who is available for the upcoming weekend. The person in charge has to wait for responses and then manually determine who can and cannot referee, and until what time. They then have to organize everything and distribute the referees to the existing games. After that, they have to submit the information to an association website and the referees receive an email notifying them of their assignments.

This process becomes slow and very confusing, especially when there are a lot of registered referees in a given area. Responses may come from one side while notifications come from another. If there is any issue, one has to go to another platform just to contact the person in question, among other things. All of this can be avoided by having everything within a single application.

##### Stakeholders

- Managers/Directors of Sports Associations
- Referees
- Project developers

##### Users

- Referee: They should be able to register and update their availability easily and quickly.
- Administrator : System administrators who have the responsibility of managing the sports associations, the referees and the general functioning of the application.
- Directors of Sports Associations: They need an efficient way to see the availability of referees and assign them to games and sporting events.
- Project Developers: They are responsible for the development, maintenance and ongoing support of the application.

***

#### Vision & Scope of the Solution

##### Vision statement

To tackle this problem, we intend to develop an application to make it easier to manage the selection of soccer referees for upcoming matches which will have information about the time, location and date of each match, as well as information about the referees registered on the app.

The application will have two types of users: "Referee", which will be a basic user (access only to essential features like selecting their range of free time) for referees who register on the application, and "Admin", which will be a user with all the permissions and features, able to manage the entire application as well as change, remove or add matches, schedules, locations, rate a referee after the match, etc.

##### List of features

- Every Friday the games of the following weekend will be attributed to the referee’s using a selection algorithm based on his rating
- Adding games
  - The administrator can add games to the system
- Adding availability time slots
  - The referee can indicate their time slots when they are available to officiate games.
- Adding a Rating to a finalized game in order to rate the referee’s performance
  - The administrator can add a rating (1-5, higher is better) to a finalized game to evaluate the referee’s performance.

##### Features that will not be developed

- Payment system for Referee (will be dealt with in a future project).

##### Risk

- Difficulty in obtaining the full participation of all referees and sports associations in adopting the application.
- Changes in sports regulations that affect the application's requirements.

##### Assumptions

- Access to a computer.
- Referees and sports associations will be willing to adopt the application.
- The information provided by referees and sports associations is accurate and up-to-date.
- The current sports regulations will remain stable during the development of the application.

***

## Requirements

#### Use Case Diagram

![Use case diagram](imgs/UML_use_case_example-800x707.png)

***

##### Use Case 1
- Actor: Actor x
- Description: Description of use case number 1
- Preconditions:
    - Precondition 1
    - Precondition 2
    - Without preconditions
- Postconditions:
    - Postcondition 1
    - Postcondition 2
    - Without postcondition
- Normal flow:
    - The user ...
    - The user ...
- Alternative flows:
    - The user ...
    - The user ...

***

##### Use Case 2

***

##### Use Case 3

***

#### User Stories and Prototypes

***

##### User Story 1
As a referee, I want to be able to easily register on the application, providing my personal information and contact details, so I can start using the system.

##### User Story 2
As a referee, I want to update my availability by specifying time slots when I am free to officiate matches, so the system can assign me to suitable games.

##### User Story 3
As a referee, I want to be able to rate my experience after officiating a match, providing feedback on the game's organization and the teams' behavior, to help improve future match assignments.

##### User Story 4
As an administrator, I want to have full control over the application, including the ability to add, modify, or remove matches from the system, so I can manage the game schedule efficiently.

##### User Story 5
As an administrator, I want to be able to view the availability of all registered referees and use an algorithm to automatically assign referees to matches based on their availability and ratings.

##### User Story 6
As an administrator, I want to rate referees after each match to evaluate their performance, providing valuable feedback to help them improve.

##### User Story 7
As an administrator, I want to be able to manage referee registration and access to the application, including the ability to suspend or remove referees who violate rules or guidelines.

##### User Story 8
As an administrator, I want to maintain an up-to-date list of match locations, including addresses and facilities, to ensure accurate match information.

###### Acceptance Criteria

```
An acceptance test should be written here
```

###### Prototype

A prototype of user story 1 should be here. You can see in (#use-case-diagram) how to import an image.

***

##### User Story 2

***

##### User Story 3

***

## Architecture and Design

#### Domain Model

![App Architecture](imgs/Arquitetura_da_Aplicacao.jpg)

- Authenticator Interface – Authenticator interface that has the purpose of handling user authentication, namely logging in and registering users in the application.
- Admin Interface – Interface for the admin that has the purpose of adding games, modifying games and removing games, managing registered referees, checking the availability of referees and evaluating referees.
- Referee Interface – Referee interface that aims to update availability, evaluate the game experience and view the games that have been selected.
- Database – System responsible for storing all application data.


***

## Implementation

#### Product Increment 1

##### Sprint Goal
The sprint goal was ...

##### Planned vs Implemented
For this iteration we planned to implement the:
- Feature 1
- Feature 2

For this iteration we implemented the:
- Feature 1
- Feature 2

##### Sprint Retrospective
- What we did well:
    - A
- What we did less well:
    - B
- How to improve to the next sprint:
    - C

***

#### Product Increment 2

***

#### Product Increment 3

***

#### Product Increment 4

***
