package pt.isec.gps2324.gps_g14.ConsoleApp;

public interface IConsoleAppModule {
    boolean menu();
    void _help();
}
